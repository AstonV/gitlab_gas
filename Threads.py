'''
---Binh Vu---
---CMPE120---
Python Threads
'''
# Importing the necessary libraries
import threading
import time

# Creating the function for increment count Thread 1
def ascending():
    count = 1
    # use while loop for decrementing count with set count = 1
    while count<11:
        print(count)
        time.sleep(1)
        count += 1

# Creating the function for decrementing count Thread 2
def descending():
    count = 10
    # use while loop for decrementing count with set count = 10
    while count>0:
        print(count)
        time.sleep(1)
        count -= 1
        
# Creating the main function
def main():
    # create two threads and assign to functions
    thread_1 = threading.Thread(target = ascending)
    thread_2 = threading.Thread(target = descending)
    # start and join threads
    thread_1.start()
    thread_2.start()
    thread_1.join()
    thread_2.join()

#Executing the main function
main()
